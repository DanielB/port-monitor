#ifndef PORTSCANNER_H_
#define PORTSCANNER_H_

#include <vector>
#include <string>

class PortScanner{

public:

	// Constructor
	PortScanner();

	// Destructor
	~PortScanner();	
	
	// Functions available for the client
	bool isPortOpen(char *ipAddress, int port);
	std::vector<int> parsePortsList(const std::string& list);
	//std::string getPortName(int port);

private:

	void swop(int &min_port, int& max_port);
	int stringToInt(const std::string &string_port);
	std::vector<int> getPortsRange(int min_port, int max_port);
	std::vector<std::string> splitPortIstruction(const std::string &port_string, char delimiter);


};




#endif  // PORTSCANNER_H_