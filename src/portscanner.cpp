
#include "../include/portscanner.h"

// SFML module for socket connection
#include <SFML/Network.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <sstream>

// Constructor
PortScanner::PortScanner(){}

// Destructor
PortScanner::~PortScanner(){}

// Class function
// Returns true if the socket is OPEN
bool PortScanner::isPortOpen(char *ip_address, int port){
    
    return (sf::TcpSocket().connect(ip_address, port) == sf::Socket::Done);

}

// Divide the string in tokens, e.g  string: 80-100,8080, token1: 80-100, token2: 8080
std::vector<std::string> PortScanner::splitPortIstruction(const std::string& port_string, char delimiter){

    std::vector<std::string> string_tokens;
    std::stringstream strStream(port_string);
    std::string token;

    bool allow_empty = false;

    while (getline(strStream, token, delimiter)) {

        // Dont allow empty string
        if (allow_empty || token.size() > 0)
            string_tokens.push_back(token);
    
    }

    return string_tokens;
}


// Only in case that min_port > max_port, e.g 80-20
// Realize the string to int conversion
int PortScanner::stringToInt(const std::string& string_port){
    std::stringstream strStream(string_port);
    int int_port;
    strStream >> int_port;
    return int_port;
}


/*template <typename T>
static void swop(T& min_port, T& max_port){
    T aux = min_port;
    min_port = max_port;
    max_port = aux;
}*/

void PortScanner::swop(int &min_port, int &max_port){
    int aux = min_port;
    min_port = max_port;
    max_port = aux;
}

// Obtain the list of ports in the port interval
std::vector<int> PortScanner::getPortsRange(int min_port, int max_port){

    std::vector<int> range_of_values;

    if(min_port > max_port)
        swop(min_port, max_port);

    if(min_port == max_port){
        range_of_values = std::vector<int>(1, min_port);
        return range_of_values;
    }else{
        
        for(int i = min_port; i <= max_port; i++){
            range_of_values.push_back(i);
        }
    }

    return range_of_values;

}

/*template <typename T>
static vector<T> getPortsRange(T min_port, T max_port)
{
    if (min_port > max_port)
        swap(min_port, max_port);
    
    if (min_port == max_port)
        return vector<T>(1, min_port);

    vector<T> values_in_range;
    for (; min_port <= max_port; ++min_port)
        values_in_range.push_back(min_port);

    return values_in_range;
}*/

std::vector<int> PortScanner::parsePortsList(const std::string& list){

    int min_port;
    int max_port;

    std::vector<int> port_list;

    if(list == "all"){
        min_port = 0;
        max_port = 65535;
                
        for (int port : getPortsRange(min_port, max_port)){
            port_list.push_back(port);
        }

    }else{

        for (const std::string& token : splitPortIstruction(list, ',')) {
        
            std::vector<std::string> strrange = splitPortIstruction(token, '-');
            switch (strrange.size()) {
                case 0: 
                    port_list.push_back(stringToInt(token));
                    break;
            
                case 1: 
                    port_list.push_back(stringToInt(strrange[0]));
                    break;

                case 2:
                    min_port = stringToInt(strrange[0]);
                    max_port = stringToInt(strrange[1]);
                    
                    for (int port : getPortsRange(min_port, max_port)){
                        port_list.push_back(port);
                    }
                    
                    break;
                default:
                    break;
            }
        }

    }
    
    return port_list;
}

