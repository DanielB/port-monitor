#include <cstring>
#include <string>
#include <vector>
#include <iostream>
#include <cstdio>
#include <cstdint>

#define LISTENING (0x0A)

#include "../include/portscanner.h"

// Max IP adress length with null  character 
#define MAX_LEN 16

int main(int argc, char const *argv[]){

    //cout << "Write the IP address: ";
    const char *ip_addr = argv[1];
    //char *ip_addr = new char[MAX_LEN];
    //fgets(ip_addr,MAX_LEN,stdin);
    char *ip_address = new char[MAX_LEN];
    strcpy(ip_address, ip_addr);

    std::string port_list;
    std::cout << "Write a Port or port list to scan(80, 80-100, 80-100,8080, all): ";
    std::cin >> port_list ; 

    std::cout << "\nIP address: " << ip_address << std::endl;
    std::cout << "Port list: " << port_list << std::endl;

    std::vector<int> ports;
    PortScanner portscan;
    ports = portscan.parsePortsList(port_list);

    std::cout << "Scanning " << ip_address << "...\n" << std::endl;
    
    std::vector<int> listen_ports;
    for (int port : ports) {
        if (portscan.isPortOpen(ip_address, port))
            std::cout << "Port " << port << "\t : LISTEN\n";
            listen_ports.push_back(port);
    }


    FILE *tcp = std::fopen("/proc/net/tcp", "r");
    int li = 1;

    uint32_t locaddr_pt1;
    uint32_t locaddr_pt2;
    uint32_t locaddr_pt3;
    uint32_t locaddr_pt4;
    uint32_t locport; 
    uint32_t state;
    char readed_line[200];

    while(fgets(readed_line, sizeof(readed_line), tcp)){
        if(li == 1){
            li++;
        }else{
            int sl = std::sscanf(readed_line, "%*d: %2x%2x%2x%2x:%4x %*8x:%*4x %2x", &locaddr_pt4,&locaddr_pt3,&locaddr_pt2,&locaddr_pt1, &locport, &state);
            //std::cout << sl << std::endl;
            std::cout << locaddr_pt1 << locaddr_pt2 << locaddr_pt3 << locaddr_pt4 << ":" << locport << std::endl;
            //std::cout << locport << std::endl;
            //std::cout << state << std::endl;
            //if(state == LISTENING){
            //    std::cout << "LISTEN" << std::endl;
            //}
            //printf("%s", readed_line );
        }
    }

    fclose(tcp);





    std::cout << std::flush;
    return 0;
}