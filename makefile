CC = g++
CFLAGS = -g -Wall
PROGRAM_NAME = ports-monitor

all: build/portscanner.o build/main.o bin/$(PROGRAM_NAME)
	@echo Compilación terminada

bin/$(PROGRAM_NAME): build/portscanner.o build/main.o
	@$(CC) $(CFLAGS) build/main.o build/portscanner.o -o bin/$(PROGRAM_NAME) -L ~/sfml/lib -lsfml-system -lsfml-network --std=c++11
	@echo Ports-monitor app is ready to use, go to src/ and execute ./run.sh + ip-address...

build/main.o: src/main.cpp include/portscanner.h
	@$(CC) $(CFLAGS) -c src/main.cpp -o build/main.o --std=c++11
	@echo main.cpp compilated...

build/portscanner.o: src/portscanner.cpp include/portscanner.h
	@$(CC) $(CFLAGS) -c src/portscanner.cpp -I ~/sfml/include/ -o build/portscanner.o --std=c++11
	@echo portscanner.cpp compilated...

clean:
	rm bin/$(PROGRAM_NAME) \
	build/main.o \
	build/portscanner.o
